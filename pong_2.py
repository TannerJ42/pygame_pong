# Import a library of functions called ‘pygame’
import pygame
from math import pi
from math import cos
from math import sin
from math import atan
from math import fabs
 
# Initialize the game engine
pygame.init()
 
# Define the colors we will use in RGB format
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
 
# Set the height and width of the screen
size = [1200, 600]
screen = pygame.display.set_mode(size)
y_velocity = 5
x_velocity = 0
#pygame.display.set_caption(“PONG”)

""" Coordinates of the scoreboard"""
scoreboard_x = 1100
scoreboard_y = 50

# create objects
top = pygame.draw.rect(screen, WHITE, [size[0] / 2, 30, 50, 20])
bottom = pygame.draw.rect(screen, WHITE, [size[0] / 2, size[1] - 50, 50, 20])
ball = pygame.draw.rect(screen, WHITE, [size[0] / 2, size[1] / 2, 10, 10])

""" Creating rectangles to mark the top and bottom of the screen"""
bottom_border = pygame.draw.rect(screen, WHITE, [0, 571, 1200, 10])
top_border = pygame.draw.rect(screen, WHITE, [0, 19, 1200, 10])

""" CREATING A FONT"""

""" None is the default font"""
font = pygame.font.Font(None, 30)

# Render the text
text = font.render("SCORE", True, WHITE, BLACK)

# Create a rectangle for the text
textRect = text.get_rect()

# Location of the rectangle
textRect.centerx = scoreboard_x
textRect.centery = scoreboard_y




# Define functions that will be used throughout the game.

def debug():
    pass

def check_for_input():
    global done
    global top_player_right
    global top_player_left
    global bottom_player_right
    global bottom_player_left
    global respawn
    
    # event.type covers if a key is depressed (KEYDOWN or KEYUP).
    # event.key checks WHAT key is depressed.
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
            done=True # Flag that we are done so we exit this loop
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_d:
                top_player_right = True
            if event.key == pygame.K_a:
                top_player_left = True
            if event.key == pygame.K_RIGHT:
                bottom_player_right = True
            if event.key == pygame.K_LEFT:
                bottom_player_left = True
            if event.key == pygame.K_UP:
                respawn = True
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_d:
                top_player_right = False
            if event.key == pygame.K_a:
                top_player_left = False
            if event.key == pygame.K_RIGHT:
                bottom_player_right = False
            if event.key == pygame.K_LEFT:
                bottom_player_left = False
            if event.key == pygame.K_UP:
                respawn = False
#        if event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
#            top_player_right = True
#        if event.type == pygame.KEYUP and event.key == pygame.K_RIGHT:
#            top_player_right = False
#        if event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
#            pressing_left = True
#        if event.type == pygame.KEYUP and event.key == pygame.K_LEFT:
#            pressing_left = False
#        if event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
#            pressing_up = True
#        if event.type == pygame.KEYUP and event.key == pygame.K_UP:
#            pressing_up = False
#        if event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
#            pressing_down = True
#        if event.type == pygame.KEYUP and event.key == pygame.K_DOWN:
#            pressing_down = False

def update():
    global y_velocity
    global x_velocity
    
    
    # Respawn function
    if respawn:
        # Set the y-coordinate for ball to the center of the y-axis.
        ball.y = size[1] / 2

        # Reset the y_velocity
        y_velocity = fabs(y_velocity)

        # Set the x-coordinate for the ball to the center of the x-axis.
        ball.x = size[0] / 2

        # Reset the x_velocity
        x_velocity = 0

    # Move ball
    ball.y = ball.y + y_velocity
    ball.x = ball.x + x_velocity

    """ Resets the ball to the top of the screen if it
    goes beyond the bottom."""
    """if ball.y > 600:
        ball.y = 0

    if ball.y < 0:
        ball.y = 600"""


    """ Resets the ball to the left or right of the screen if it
        goes beyond the right or left border"""
    if ball.x > 1200:
        ball.x = 0

    if ball.x < 0:
        ball.x = 1200


    # See if the ball hits either panel.   
    # top panel
    if collision_check(ball, top):
        y_velocity = fabs(y_velocity) # bounce ball down.
        if top_player_left:
            x_velocity = -5 # bounce ball left
        if top_player_right:
            x_velocity = 5# bounce ball right
    # bottom panel
    if collision_check(ball, bottom):
        y_velocity = -1 * fabs(y_velocity) # bounce ball up.
        if bottom_player_left:
            x_velocity = -5 # bounce ball left
        if bottom_player_right:
            x_velocity = 5 # bounce ball right

    """ See if ball hits the top border"""
    if collision_check(ball, top_border):
        y_velocity = fabs(y_velocity) # bounce ball down.
        # Set the y-coordinate for ball to the center of the y-axis.
        ball.y = size[1] / 2

        # Reset the y_velocity
        y_velocity = fabs(y_velocity)

        # Set the x-coordinate for the ball to the center of the x-axis.
        ball.x = size[0] / 2

        # Reset the x_velocity
        x_velocity = 0
    """ See if ball hits the top border"""    
    if collision_check(ball, bottom_border):
        y_velocity = fabs(y_velocity) # bounce ball down.
        # Set the y-coordinate for ball to the center of the y-axis.
        ball.y = size[1] / 2

        # Reset the y_velocity
        y_velocity = fabs(y_velocity)

        # Set the x-coordinate for the ball to the center of the x-axis.
        ball.x = size[0] / 2

        # Reset the x_velocity
        x_velocity = 0

    

    # Move panels based on input.
    # Pressing Right Arrow makes it go Right.
    if top_player_right:
        top.x = top.x + 5
    # Pressing Left Arrow makes it go Left.
    if top_player_left:
        top.x = top.x - 5
    # Pressing Up Arrow makes bottom block go Left.
    if bottom_player_right:
        bottom.x = bottom.x + 5
    # Pressing Down Arrow makes bottom block to Right.
    if bottom_player_left:
        bottom.x = bottom.x - 5

def draw():
    # All drawing code happens after the for loop and but
    # inside the main while done==False loop.
    # Clear the screen and set the screen background
    screen.fill(BLACK)
 
    # Draw rectangles
    pygame.draw.rect(screen, WHITE, top)
    pygame.draw.rect(screen, WHITE, bottom)
    pygame.draw.rect(screen, WHITE, ball)
    pygame.draw.rect(screen, WHITE, bottom_border)
    pygame.draw.rect(screen, WHITE, top_border)
    pygame.draw.rect(screen, RED, textRect)

    """ Blit the screen, draws the text score on the screen"""
    screen.blit(text, textRect)

    # Go ahead and update the screen with what we’ve drawn.
    # This MUST happen after all the other drawing commands.
    pygame.display.flip()

# Returns TRUE when two tops collide.
def collision_check (top1, top2):
    return (top1.colliderect(top2))

# Variables to check what key is being depressed.
top_player_right = False
top_player_left = False
bottom_player_right = False
bottom_player_left = False
pressing_a = False

respawn = False

#Loop until the user clicks the close button.
done = False
clock = pygame.time.Clock()

""" This is the game loop. We'll iterate through each object, update it,
then at the end draw everything. We'll keep looping until the game is over."""
while not done:
    # This limits the while loop to a max of 30 times per second.
    # Leave this out and we will use all CPU we can.
    clock.tick(30)

    check_for_input()

    debug()

    update()

    draw()

# Be IDLE friendly
pygame.quit()
